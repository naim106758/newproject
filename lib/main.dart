import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  int leftCardNuber=3;
  int rightCardNuber=5;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
         body: Container(
           height:double.infinity,
           width:double.infinity,
           decoration: const BoxDecoration(
             image: DecorationImage(
               fit: BoxFit.cover,
               image: AssetImage('assets/images/background.jpg'),
             )
           ),
           child: SafeArea(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                  child: Image.asset(
                    "assets/images/logo.jpeg",
                    width: 200,
                    height: 200,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Image.asset(
                      'assets/images/card$leftCardNuber.png',
                      fit: BoxFit.cover,
                    ),
                    const SizedBox(
                      width: 40,
                    ),
                    Image.asset(
                      'assets/images/card$rightCardNuber.png',
                      fit: BoxFit.cover,
                    ),
                  ],
                ),
                TextButton(onPressed: (){
                  setState(() {
                    leftCardNuber=8;
                    rightCardNuber=7;
                  });
                }, child: Image.asset('assets/images/dealbutton.png'),),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Column(
                      children: const [
                         Text("Player",style: TextStyle(fontSize: 25,color: Colors.cyan),),
                         Text("0",style: TextStyle(fontSize: 25,color: Colors.cyan),)
                      ],
                    ),
                    Column(
                      children: const [
                        Text("CPU",style: TextStyle(fontSize: 25,color: Colors.cyan),),
                        Text("0",style: TextStyle(fontSize: 25,color: Colors.cyan),)
                      ],
                    )
                  ],
                )
              ],
            ),
        ),
         ),
      ),
    );
  }
}
